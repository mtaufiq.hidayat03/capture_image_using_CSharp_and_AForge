﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Net;
using System.Timers;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using MaterialSkin;
using MaterialSkin.Controls;
using ZXing;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;

namespace WindowsFormsApp1
{
    public partial class QrCodeScanner : MaterialSkin.Controls.MaterialForm 
    {
        public QrCodeScanner()
        {
            InitializeComponent();
            txtQrCode.ReadOnly = true;
            /*
            MaterialSkin.MaterialSkinManager skinManager = MaterialSkin.MaterialSkinManager.Instance;  
            skinManager.AddFormToManage(this);  
            skinManager.Theme = MaterialSkin.MaterialSkinManager.Themes.LIGHT;
            skinManager.ColorScheme = new MaterialSkin.ColorScheme(MaterialSkin.Primary.Red900, 
                MaterialSkin.Primary.Pink900, MaterialSkin.Primary.Red900, Accent.Red700, 
                MaterialSkin.TextShade.WHITE);
            */
        }
        
        FilterInfoCollection filterInfoCollection;
        VideoCaptureDevice videoCaptureDevice;
        private string FilePath { get; set; }
        private Microsoft.Office.Interop.PowerPoint.Application _ppApp;
        private Presentations _ppPresentations;
        private Presentation _objPress;
        private Microsoft.Office.Interop.PowerPoint.SlideShowWindows _objSSWs;
        private Microsoft.Office.Interop.PowerPoint.SlideShowSettings _objSSS;
        private string resultScanner;
        private void QrCodeScanner_Load(object sender, EventArgs e)
        {
            filterInfoCollection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo filterInfo in filterInfoCollection)
                cboCamera.Items.Add(filterInfo.Name);
            cboCamera.SelectedIndex = 0;
            videoCaptureDevice = new VideoCaptureDevice();
            //mtlBtnStop.Enabled = false;
        }

        /* private void btnStart_Click(object sender, EventArgs e)
        {
            videoCaptureDevice = new VideoCaptureDevice(filterInfoCollection[cboCamera.SelectedIndex].MonikerString);
            videoCaptureDevice.NewFrame += VideoCaptureDevice_NewFrame;
            videoCaptureDevice.Start();
            timer1.Start();
            mtlBtnStart.Enabled = false;
            mtlBtnStop.Enabled = true;
        } */
        
        private void VideoCaptureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            pictureBox1.Image = (Bitmap) eventArgs.Frame.Clone();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void QrCodeScanner_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (videoCaptureDevice.IsRunning == true)
                videoCaptureDevice.Stop();
        }

        private void timer1_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                BarcodeReader barcodeReader = new BarcodeReader();
                Result result = barcodeReader.Decode((Bitmap)pictureBox1.Image);
                if (result != null)
                {
                    resultScanner = result.ToString();
                    txtQrCode.Text = resultScanner;
                    WebClient webClient = new WebClient();
                    webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                    webClient.DownloadFileAsync(new Uri("http://127.0.0.1:805/"+resultScanner+".ppt"),@"D:\"+resultScanner+".ppt");
                    timer1.Stop();
                }
            }
        }
        
        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            MessageBox.Show("Download Completed!");
            FilePath = @"D:\"+resultScanner+".ppt";
            _ppApp = new Microsoft.Office.Interop.PowerPoint.Application();
            _ppApp.Visible = MsoTriState.msoTrue;
            _ppPresentations = _ppApp.Presentations;
            _objPress = _ppPresentations.Open(FilePath, MsoTriState.msoFalse, MsoTriState.msoTrue,
                MsoTriState.msoTrue);
            Slides objSlides = _objPress.Slides;
            _objSSS = _objPress.SlideShowSettings;
            _objSSS.Run();
            _objSSWs = _ppApp.SlideShowWindows;
            Console.Out.Write(_objSSWs.Count);
            while (_objSSWs.Count >= 1)
            {
                System.Threading.Thread.Sleep(100);
            }
            _objPress.Close();
            _ppApp.Quit();
        }
        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            progressBar1.Text = e.ProgressPercentage.ToString();
        }

        private void mtlBtnStart_Click(object sender, EventArgs e)
        {
            videoCaptureDevice = new VideoCaptureDevice(filterInfoCollection[cboCamera.SelectedIndex].MonikerString);
            videoCaptureDevice.NewFrame += VideoCaptureDevice_NewFrame;
            videoCaptureDevice.Start();
            timer1.Start(); 
            mtlBtnStart.Enabled = false;
            mtlBtnStop.Enabled = true;
        }

        private void mtlBtnStop_Click(object sender, EventArgs e)
        {
            videoCaptureDevice.Stop();
            mtlBtnStart.Enabled = true;
            mtlBtnStop.Enabled = false;
        }

        private void materialLabel1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            //throw new System.NotImplementedException();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            //throw new System.NotImplementedException();
        }

        private void cboCamera_SelectedIndexChanged(object sender, EventArgs e)
        {
           // throw new System.NotImplementedException();
        }
    }
}