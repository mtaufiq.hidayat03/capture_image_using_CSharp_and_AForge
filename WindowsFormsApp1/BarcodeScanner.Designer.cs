﻿using System.ComponentModel;

namespace WindowsFormsApp1
{
    partial class QrCodeScanner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QrCodeScanner));
            this.label1 = new System.Windows.Forms.Label();
            this.cboCamera = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtQrCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Timers.Timer();
            this.mtlBtnStart = new MaterialSkin.Controls.MaterialButton();
            this.mtlBtnStop = new MaterialSkin.Controls.MaterialButton();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.timer1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(40, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Camera Option";
            // 
            // cboCamera
            // 
            this.cboCamera.FormattingEnabled = true;
            this.cboCamera.Location = new System.Drawing.Point(242, 94);
            this.cboCamera.Name = "cboCamera";
            this.cboCamera.Size = new System.Drawing.Size(730, 24);
            this.cboCamera.TabIndex = 1;
            this.cboCamera.SelectedIndexChanged += new System.EventHandler(this.cboCamera_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (192)))), ((int) (((byte) (255)))), ((int) (((byte) (255)))));
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image) (resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(242, 138);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(730, 464);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // txtQrCode
            // 
            this.txtQrCode.Location = new System.Drawing.Point(242, 625);
            this.txtQrCode.Name = "txtQrCode";
            this.txtQrCode.Size = new System.Drawing.Size(730, 22);
            this.txtQrCode.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(40, 628);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "Barcode Result";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000D;
            this.timer1.SynchronizingObject = this;
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Elapsed);
            // 
            // mtlBtnStart
            // 
            this.mtlBtnStart.AutoSize = false;
            this.mtlBtnStart.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mtlBtnStart.Depth = 0;
            this.mtlBtnStart.DrawShadows = true;
            this.mtlBtnStart.HighEmphasis = true;
            this.mtlBtnStart.Icon = null;
            this.mtlBtnStart.Location = new System.Drawing.Point(41, 165);
            this.mtlBtnStart.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mtlBtnStart.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtlBtnStart.Name = "mtlBtnStart";
            this.mtlBtnStart.Size = new System.Drawing.Size(145, 36);
            this.mtlBtnStart.TabIndex = 6;
            this.mtlBtnStart.Text = "START";
            this.mtlBtnStart.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.mtlBtnStart.UseAccentColor = false;
            this.mtlBtnStart.UseVisualStyleBackColor = true;
            this.mtlBtnStart.Click += new System.EventHandler(this.mtlBtnStart_Click);
            // 
            // mtlBtnStop
            // 
            this.mtlBtnStop.AutoSize = false;
            this.mtlBtnStop.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mtlBtnStop.Depth = 0;
            this.mtlBtnStop.DrawShadows = true;
            this.mtlBtnStop.HighEmphasis = true;
            this.mtlBtnStop.Icon = null;
            this.mtlBtnStop.Location = new System.Drawing.Point(41, 241);
            this.mtlBtnStop.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mtlBtnStop.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtlBtnStop.Name = "mtlBtnStop";
            this.mtlBtnStop.Size = new System.Drawing.Size(145, 36);
            this.mtlBtnStop.TabIndex = 7;
            this.mtlBtnStop.Text = "STOP";
            this.mtlBtnStop.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.mtlBtnStop.UseAccentColor = false;
            this.mtlBtnStop.UseVisualStyleBackColor = true;
            this.mtlBtnStop.Click += new System.EventHandler(this.mtlBtnStop_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(41, 683);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(931, 23);
            this.progressBar1.TabIndex = 8;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // QrCodeScanner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 767);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.mtlBtnStop);
            this.Controls.Add(this.mtlBtnStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtQrCode);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cboCamera);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "QrCodeScanner";
            this.RightToLeftLayout = true;
            this.Text = "Barcode Scanner Using Camera";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.QrCodeScanner_FormClosing);
            this.Load += new System.EventHandler(this.QrCodeScanner_Load);
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.timer1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.ProgressBar progressBar1;

        private MaterialSkin.Controls.MaterialButton mtlBtnStop;

        private MaterialSkin.Controls.MaterialButton mtlBtnStart;

        private System.Timers.Timer timer1;

        private System.Windows.Forms.Label label2;

        private System.Windows.Forms.TextBox txtQrCode;

        private System.Windows.Forms.PictureBox pictureBox1;

        private System.Windows.Forms.ComboBox cboCamera;

        private System.Windows.Forms.Label label1;

        #endregion
    }
}